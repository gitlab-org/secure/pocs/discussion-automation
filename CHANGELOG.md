# CHANGELOG

| Type    | MR                                                                                  | Description                                        |
|---------|-------------------------------------------------------------------------------------|----------------------------------------------------|
| Feature | https://gitlab.com/gitlab-org/secure/pocs/discoto/-/merge_requests/23 | Bump gem dependencies                              |
| Bug     | https://gitlab.com/gitlab-org/secure/pocs/discoto/-/merge_requests/21 | Exclude internal notes from auto-summary           |
| Feature | https://gitlab.com/gitlab-org/secure/pocs/discoto/-/merge_requests/15 | Add Discoto configuration in YAML code block       |
| Bug     | https://gitlab.com/gitlab-org/secure/pocs/discoto/-/merge_requests/14 | Quick actions weren't stripped from copied threads |
