# Discoto

[[_TOC_]]

Discoto (A mix of "*DISC*ussion" and "*AUTO*mation") is a Discussion Automation
tool. It automatically creates and propagates discussion summaries and helps
keep discussions organized.

Currently this works on issues, epics, and merge requests.

| ![Example Discussion](./discoto_example.gif) |
|---|

| Demo: [Recording (internal)](https://drive.google.com/file/d/1zrU93SM8FalrBBnHgN-rYDGXH5YI6d9j/view) | [Slides (internal)](https://docs.google.com/presentation/u/0/d/1VETxcanz3ad1Jqk-W_Za6Cp0yZEsTHs5RqthQ_OmX1E/edit) |
|---|---|

## Getting Started

Follow the steps below to get started:

1. (optional) Create a new issuable (issue/epic/MR)
1. Add either ~discoto (stable) or ~"discoto-beta" (maybe unstable) to the issuable
1. Use discoto features!

**Topics**

Topics can be stand-alone (an epic, issue, MR that has a discoto label), or can
be inline.

Inline topics are are discussions within an issuable
where the first line of the first note is a heading that starts
with `topic:`. For example, the following are all
valid topics:

* `# Topic: Inline discussion topic 1`
* `## TOPIC: **{+A Green, bolded topic+}**`
* `### tOpIc: Another topic`

**Points**

Main discussion points are propagated into the auto-generated summary. They
are declared by headings, list items, and single
lines that start with the text `point:`. For
example, the following are all valid points:

* `#### POINT: This is a point`
* `* point: This is a point`
* `+ Point: This is a point`
* `- pOINT: This is a point`
* `point: This is a **point**`

Note that any markdown used in the point text will also be propagated
into the topic summaries.

**Quick Actions**

| Action                        | Description                                             |
|-------------------------------|---------------------------------------------------------|
| `/discuss sub-topic TITLE`    | Create an issue for a sub-topic. Does not work in epics |
| `/discuss link ISSUABLE-LINK` | Link an issuable as a child of this discussion          |

### Settings Schema

Issuables are able to configure the appearance of the Discoto summary by editing
values in the `Discoto Settings` YAML code block:

| [![Animation showing the YAML settings block controlling the number of items in the summary](discoto_settings_yaml.gif)](discoto_settings_yaml.gif) |
|---|

Below are the default values for the settings:

```yaml
summary:
  max_items: -1              # An integer. Values less than zero mean no limit
  sort_by: created           # One of created, updated
  sort_direction: ascending  # One of ascending, descending
```

The current schema for the settings YAML is:

```json
{
  "type": "object",
  "properties": {
    "max_items": {
      "type": "integer",
      "default": -1
    },
    "sort_by": {
      "type": "string",
      "enum": [ "created", "updated" ],
      "default": "created"
    },
    "sort_direction": {
      "type": "string",
      "enum": [ "ascending", "descending" ],
      "default": "ascending"
    }
  }
}
```

### Setup

Include the Discoto CI include in your project's `.gitlab-ci.yml`:

```yaml
include:
  - project: gitlab-org/secure/pocs/discoto
    file: /ci_includes/Discoto.gitlab-ci.yml
```

The included YAML defines a job, `discoto`, that runs only if the `$DISCOTO`
variable is set.

Create a scheduled pipeline in your project that runs at your desired intervals.
The pipeline *must* set the required variables listed in the table below:

| Name                        | Required           | Default              | Note                                                       |
|-----------------------------|--------------------|----------------------|------------------------------------------------------------|
| `DISCOTO`                   | :heavy_check_mark: |                      | Required for included job to run                           |
| `DISCOTO_GITLAB_TOKEN`      | :heavy_check_mark: |                      | Required for GitLab API use                                |
| `DISCOTO_PROJECTS`          | :heavy_check_mark: |                      | Comma-separated project paths for issues and MRs           |
| `DISCOTO_GROUPS`            | :heavy_check_mark: |                      | Comma-separated group paths for epics                      |
| `DISCOTO_GITLAB_ENDPOINT`   |                    | `https://gitlab.com` |                                                            |
| `DISCOTO_ISSUABLE_ASSIGNEE` |                    |                      | Only load initial topics with this assignee username       |
| `DISCOTO_ISSUABLE_AUTHOR`   |                    |                      | Only load initial topics with this author username         |
| `DISCOTO_ISSUABLE_LABELS`   |                    | `discoto`            | Only load initial topics with these comma-separated labels |

**Groups and Projects**

Topics loaded from merge requests and issues are only loaded from the
projects listed in the `DISCOTO_PROJECTS` variable. The `DISCOTO_GROUPS`
variable is only used to determine from which groups epics will be loaded.

**Initial Topic Loading**

The `DISCOTO_ISSUABLE_*` variables are used to filter the **initial**
topics that are loaded. Discoto will still load all sub-topics of the initially
loaded topics, even if the issuable filters do not match.

For example, suppose the filter `DISCOTO_ISSUABLE_ASSIGNEE` was set to
`discoto-test-user`. Only topics that are assigned to `discoto-test-user`
would initially be loaded. If a sub-topic of one of the initial topics was
assigned to someone else, it would still be loaded since Discoto had previously
linked directly to it.

## How it Works

When Discoto runs, it:

* Loads all open discussion topics (issues in the project that have the ~discoto label)
* Loads all threads in each topic
* Evaluates quick actions
* Updates topic summaries
* Propagates topic summaries to parent topics

See the [terminology](#terminology) and [details](#details) sections for more
details.

## Details

Sub-topic linking is performed by persisting information about the links in
JSON in html comments in the "Discussion Continues" comment. Below is an example:

```
### Discussion Continues

| [:arrow_right:](https://gitlab.com/d0c-s4vage-tests/divide-and-conquer-discussions/-/issues/47) https://gitlab.com/d0c-s4vage-tests/divide-and-conquer-discussions/-/issues/47 |
|---|


<!-- discoto: {"sub_topic":{"url":"https://gitlab.com/d0c-s4vage-tests/divide-and-conquer-discussions/-/issues/47","type":"topic","issuable_type":"issue","project_id":21300268,"iid":47}} -->
```

### Terminology

**Topic**

* A topic is a single issue for a discussion
* Topics can have any number of sub-topics, both inline and distinct topics
* All topic issues have the label ~discoto

**Inline Topic**

* Inline topics are discussion threads within the comments of an existing topic
* The first comment of a thread must start with `# Topic: `

**Thread**

* A thread is a series of comments in a discussion

## Developer Documentation

Please read the [development documentation](DEVELOPMENT.md).

## GitLab-Specific (Internal) Usage

Within gitlab-org and gitlab-com, adding one of the labels below will
enable Discoto on an issuable:

* ~discoto - Stable version of discoto
* ~discoto-beta - Potentially unstable, cutting-edge features of Discoto

If Discoto does not seem to be updating an issuable, consider [creating an issue](https://gitlab.com/gitlab-org/secure/pocs/discoto-runner/-/issues/new?issuable_template=AddNamespaceOrGroupToPipeline&issue[title]=Add%20Project/Group%20To%20Scheduled%20Pipeline)
to add the project/group to the scheduled pipeline.

<details>
<summary><b>GitLab-Specific Setup Details</b></summary>
<br/>

Discoto runs on gitlab-org and gitlab-com issuables via scheduled pipelines
in the [discoto-runner](https://gitlab.com/gitlab-org/secure/pocs/discoto-runner)
project.

Currently, Discoto *does* need to look at specific groups and projects to
identify issuables with the discoto labels. If discoto-runner does not seem to be
handling issuables within a project or group, please [create an issue
in the discoto-runner project](https://gitlab.com/gitlab-org/secure/pocs/discoto-runner/-/issues/new?issuable_template=AddNamespaceOrGroupToPipeline&issue[title]=Add%20Project/Group%20To%20Scheduled%20Pipeline) to update the scheduled pipeline to include
the missing group/project:

https://gitlab.com/gitlab-org/secure/pocs/discoto-runner/-/issues/new?issuable_template=AddNamespaceOrGroupToPipeline&issue[title]=Add%20Project/Group%20To%20Scheduled%20Pipeline

</details>

