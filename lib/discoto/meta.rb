# frozen_string_literal: true

require 'json'

module Discoto
  module Meta
    PREFIX = '<!-- discoto: '
    POSTFIX = ' -->'

    def self.extract_meta(markdown)
      match = markdown.match(/.*#{PREFIX}(.*)#{POSTFIX}.*/m)
      return nil if match.nil?

      JSON.parse(match.captures[0])
    end

    def self.inject_meta(markdown, data)
      json_data = JSON.generate(data)
      markdown + "\n\n#{PREFIX}#{json_data}#{POSTFIX}"
    end

    # ------------

    def self.create_visible_markers(type)
      type = type.gsub(/[^a-zA-Z0-9_-]/, '-').gsub(/--*/, '-')
      marker_prefix = "<!-- start-discoto-#{type} -->"
      marker_postfix = "<!-- end-discoto-#{type} -->"
      [marker_prefix, marker_postfix]
    end

    def self.extract_visible(markdown, type)
      prefix, postfix = create_visible_markers(type)
      match = markdown.match(/#{prefix}(.*)#{postfix}/m)
      return nil if match.nil?

      match.captures[0]
    end

    def self.inject_visible(markdown, type, data)
      prefix, postfix = create_visible_markers(type)
      match = markdown.match(/#{prefix}.*#{postfix}/m)
      if match.nil?
        return "#{markdown}\n\n#{prefix}#{data}#{postfix}"
      end

      start_offset, end_offset = match.offset(0)
      markdown[0..start_offset - 1] + prefix + data + postfix + markdown[end_offset..]
    end
  end
end
