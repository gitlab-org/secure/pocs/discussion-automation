# frozen_string_literal: true

module Discoto
  module Items
    class MainPoint
      attr_accessor :text

      def initialize(loader, text, note, issuable)
        @loader = loader
        @text = text
        @issuable = issuable
        @note = note
      end

      def url
        @loader.url_for_note(@issuable, @note)
      end

      def created
        DateTime.parse(@note['created_at'])
      end

      def updated
        DateTime.parse(@note['updated_at'])
      end
    end
  end
end
