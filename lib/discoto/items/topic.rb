# frozen_string_literal: true

require_relative '../mixins/summary'
require_relative '../mixins/topic_tree'
require_relative '../config'
require_relative '../quick_actions'
require_relative '../utils'
require_relative 'thread'

module Discoto
  module Items
    SUMMARY_PREFIX = '<!-- start-discoto-summary -->'
    SUMMARY_POSTFIX = '<!-- end-discoto-summary -->'
    SUMMARY_MATCH = /.*#{SUMMARY_PREFIX}(.*)#{SUMMARY_POSTFIX}.*/m.freeze

    class Topic
      include Mixins::Summary
      include Mixins::TopicTree

      attr_accessor \
        :threads,
        :sub_topics,
        :parent_topic,
        :processed,
        :updated_summary,
        :issuable,
        :settings

      def self.create(loader, project_id, title, description: nil)
        # sub-topics are always new issues (for now)
        new_issue = loader.create_issue(
          project_id,
          title,
          description: description
        )
        Topic.new(loader, new_issue)
      end

      def self.create_from_meta(loader, meta)
        container_id = meta['container_id']
        container_type = meta['container_type']
        issuable_iid = meta['iid']
        issuable_type = meta['issuable_type']
        loader.load_topic(issuable_type, issuable_iid, container_type, container_id)
      end

      def initialize(loader, issuable)
        @issuable = issuable
        @loader = loader
        @updated_summary = false
        @actions = ::Discoto::QuickActions.new(@loader)
        @parent_topic = nil
        @processed = false
        @summarized = false

        @settings = ::Discoto::TopicSettings.new_from_meta(@issuable.description)
        @settings ||= ::Discoto::TopicSettings.new

        @sub_topics = []

        @threads = @loader.load_discussions(@issuable).map do |disc|
          Thread.new(loader, disc, self)
        end
      end

      def confidential?
        if @issuable.type != 'merge_request' && @issuable.confidential
          return true
        end

        cinfo = @loader.container_info(@issuable)
        container = @loader.load_container(cinfo)
        container.visibility == 'private'
      end

      def created
        DateTime.parse(@issuable['created_at'])
      end

      def updated
        DateTime.parse(@issuable['updated_at'])
      end

      def meta
        cinfo = @loader.container_info(@issuable)
        {
          'url' => url,
          'type' => 'topic',
          'container_id' => cinfo.id,
          'container_type' => cinfo.type,
          'issuable_type' => @issuable.type,
          'iid' => @issuable.iid
        }
      end

      def title
        @issuable.title
      end

      def url
        @issuable.web_url
      end

      def process
        @processed = true

        @sub_topics = []
        @threads.each do |thread|
          thread.process(@actions)
          add_sub_topics(thread.topics)
        end
        add_sub_topics(@actions.new_sub_topics)

        total_sub_topics = []
        @sub_topics.each do |topic|
          topic.parent_topic = self
          next if topic.processed

          total_sub_topics += topic.process
        end

        [self, total_sub_topics].flatten
      end

      def add_sub_topics(new_topics)
        seen = Set.new(@sub_topics)
        new_topics.each do |topic|
          next if seen.include? topic

          @sub_topics << topic
          seen.add(topic)
        end
      end

      def topic_threads
        @threads
      end

      # create a new thread (discussion) with the given title
      def create_thread(title)
        new_discussion = @loader.create_discussion(@issuable, title)
        new_thread = Thread.new(@loader, new_discussion, self)
        @threads << new_thread
        new_thread
      end

      def break_cycles!
        seen = Set.new
        sub_topics_to_search = [] + @sub_topics
        while sub_topics_to_search.count.positive?
          curr_topic = sub_topics_to_search.pop
          seen.add(curr_topic)
          if curr_topic == self
            curr_topic.parent_topic.sub_topics.delete(self)
            # saw ourself, let's assume that we're the root then
            @parent_topic = nil
          end

          curr_topic.sub_topics.each do |sub_sub_topic|
            next if seen.include?(sub_sub_topic)

            sub_topics_to_search << sub_sub_topic
          end
        end
      end

      def main_points
        @threads.map do |thread|
          if thread.topic?
            []
          else
            thread.main_points
          end
        end.reduce([], :concat) || []
      end

      def update_summary
        return if @updated_summary

        @updated_summary = true
        root = root_topic

        backlinks = []
        unless root.nil?
          backlinks << "* :arrow_double_up: **ROOT** #{summary_title(root)}"
        end
        unless parent_topic.nil? || parent_topic == root
          backlinks << "* :arrow_backward: **PARENT** #{summary_title(parent_topic)}"
        end

        pipeline_info_desc = 'Last updated by'
        pipeline_info = Utils.default_env('CI_JOB_URL', '')
        unless pipeline_info.empty?
          pipeline_info = "#{pipeline_info_desc} [this job](#{pipeline_info})"
        end

        newline = "\n"
        new_summary = <<~TEXT
          #{newline}## Auto-Summary :robot:


          <details>
          <summary>Discoto Usage</summary>

          ---

          > **Points**
          >
          > Discussion points are declared by headings, list items, and single
          > lines that start with the text (case-insensitive) `point:`. For
          > example, the following are all valid points:
          >
          > * `#### POINT: This is a point`
          > * `* point: This is a point`
          > * `+ Point: This is a point`
          > * `- pOINT: This is a point`
          > * `point: This is a **point**`
          >
          > Note that any markdown used in the point text will also be propagated
          > into the topic summaries.
          >
          > **Topics**
          >
          > Topics can be stand-alone and contained within an issuable (epic,
          > issue, MR), or can be inline.
          >
          > Inline topics are defined by creating a new thread (discussion)
          > where the first line of the first comment is a heading that starts
          > with (case-insensitive) `topic:`. For example, the following are all
          > valid topics:
          >
          > * `# Topic: Inline discussion topic 1`
          > * `## TOPIC: **{+A Green, bolded topic+}**`
          > * `### tOpIc: Another topic`
          >
          > **Quick Actions**
          >
          > | Action                        | Description                                             |
          > |-------------------------------|---------------------------------------------------------|
          > | `/discuss sub-topic TITLE`    | Create an issue for a sub-topic. Does not work in epics |
          > | `/discuss link ISSUABLE-LINK` | Link an issuable as a child of this discussion          |
          >

          ---

          </details>

          #{pipeline_info}

          #{backlinks.join("\n")}
          #{summary_lines.join("\n")}
        TEXT

        # the description *can* be nil if it has never been set
        curr_desc = @issuable.description.to_s
        summary_match = curr_desc.match(SUMMARY_MATCH)
        if summary_match.nil?
          new_desc = curr_desc + "\n\n#{SUMMARY_PREFIX}#{new_summary}#{SUMMARY_POSTFIX}\n\n"
        else
          start_offset, end_offset = summary_match.offset(1)
          new_desc = curr_desc[0..start_offset - 1] + new_summary + curr_desc[end_offset..]
        end

        # keep the settings up-to-date!
        new_desc = settings.update_or_inject_meta(new_desc)

        masked_new_desc = new_desc.gsub(/^#{pipeline_info_desc}.*/, '')
        masked_curr_desc = curr_desc.gsub(/^#{pipeline_info_desc}.*/, '')

        return if masked_new_desc == masked_curr_desc

        @issuable = @loader.set_issuable_description(@issuable, new_desc)
      end

      def copy_thread(thread)
        thread_link = @loader.url_for_note(thread.issuable, thread.notes[0])
        discussion = @loader.create_discussion(@issuable, "Copied from [thread](#{thread_link})")
        new_thread = Thread.new(@loader, discussion, self)

        thread.notes.each do |note|
          new_note_text = Discoto::QuickActions.remove_quick_actions(note['body'])
          next if new_note_text.strip.empty?

          orig_note_url = @loader.url_for_note(thread.issuable, note)
          new_note_text.gsub!(/^#+\s+topic:/i, 'Topic:')
          new_thread.add_note("[From](#{orig_note_url}) @#{note['author']['username']}:\n\n#{new_note_text}")
        end

        @threads.push(new_thread)
      end

      def to_s
        "<Topic url=#{url}>"
      end
    end
  end
end
