# frozen_string_literal: true

require_relative '../topic_settings'
require_relative '../mixins/summary'
require_relative '../mixins/topic_tree'

module Discoto
  module Items
    class InlineTopic
      include Mixins::Summary
      include Mixins::TopicTree

      attr_accessor :thread, :sub_topics, :parent_topic, :processed, :updated_summary, :settings

      def initialize(loader, thread)
        @loader = loader
        @actions = ::Discoto::QuickActions.new(@loader)
        @thread = thread
        @sub_topics = []
        @parent_topic = nil
        @processed = false
        @updated_summary = false
        # only available on issuables, not inline topics - we use the default
        # settings for inline topics
        @settings = ::Discoto::TopicSettings.new
      end

      def confidential?
        @parent_topic.confidential?
      end

      def created
        thread.created
      end

      def updated
        thread.updated
      end

      def process
        @processed = true

        @actions.process(@thread)
        add_sub_topics(@actions.new_sub_topics)

        res = []

        @sub_topics.each do |topic|
          topic.parent_topic = self
          next if topic.processed

          res += topic.process
        end

        [self, res].flatten
      end

      def add_sub_topics(new_topics)
        seen = Set.new(@sub_topics)
        new_topics.each do |topic|
          next if seen.include? topic

          @sub_topics << topic
          seen.add(topic)
        end
      end

      def break_cycles!; end

      def main_points
        @thread.main_points
      end

      def title
        @thread.topic_title
      end

      def url
        @thread.url
      end

      def update_summary
        # noop
      end

      def to_s
        "<InlineTopic url=#{url}>"
      end
    end
  end
end
