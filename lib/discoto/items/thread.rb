# frozen_string_literal: true

require 'json'
require 'date'

require_relative '../meta'
require_relative 'inline_topic'
require_relative 'main_point'

module Discoto
  module Items
    class Thread
      INLINE_TOPIC_MATCH = /^#+ +topic\s*[:-]\s*(.*)$/i.freeze
      POINT_MATCH = /^[\s#+*-]*point\s*[:-]\s*(.*)$/i.freeze

      attr_accessor :main_points

      def initialize(loader, discussion, topic)
        @loader = loader
        @discussion = discussion
        @topic = topic
        @main_points = []
      end

      def process(actions)
        actions.process(self) unless topic?
        @main_points = find_main_points
      end

      def issuable
        @discussion.issuable
      end

      def notes
        @discussion.notes
      end

      def created
        # notes are always sorted oldest-first!
        DateTime.parse(notes.first.created_at)
      end

      def updated
        # notes are always sorted oldest-first! most recent last!
        DateTime.parse(notes.last.updated_at)
      end

      def topic?
        @discussion.notes[0]['body'].strip =~ INLINE_TOPIC_MATCH
      end

      def topic_title
        @discussion.notes[0]['body'].match(INLINE_TOPIC_MATCH).captures[0].strip
      end

      def topics
        res = []

        sub_topics = find_sub_topics
        if topic?
          inline_topic = InlineTopic.new(@loader, self)
          inline_topic.add_sub_topics(sub_topics)
          res.push(inline_topic)
        else
          res += sub_topics
        end

        res
      end

      def find_main_points
        res = []
        @discussion.notes.each do |note|
          note['body'].scan(POINT_MATCH).each do |point|
            res.push(MainPoint.new(@loader, point[0].strip, note, issuable))
          end
        end
        res
      end

      def find_sub_topics
        res = []
        @discussion.notes.each do |note|
          meta = Meta.extract_meta(note['body'])
          next if meta.nil?

          topic = Items::Topic.create_from_meta(@loader, meta['sub_topic'])
          res.push(topic) unless topic.nil?
        end
        res
      end

      def url
        @loader.url_for_note(issuable, @discussion.notes[0])
      end

      def add_link!(existing_topic)
        topic_url = existing_topic.url
        note_text = <<~TEXT
          ### Discussion Linked

          | [:arrow_right:](#{topic_url}) #{topic_url} |
          |---|
        TEXT
        note_text = Meta.inject_meta(note_text, { sub_topic: existing_topic.meta })
        add_note note_text
      end

      def close!(moved_to_topic)
        topic_url = moved_to_topic.url
        note_text = <<~TEXT
          ### Discussion Continues

          | [:arrow_right:](#{topic_url}) #{topic_url} |
          |---|
        TEXT
        note_text = Meta.inject_meta(note_text, { sub_topic: moved_to_topic.meta })
        add_note note_text
      end

      def add_note(body)
        @loader.add_note_to_discussion(@discussion, body)
      end
    end
  end
end
