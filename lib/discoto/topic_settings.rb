# frozen_string_literal: true

require 'yaml'
require 'json-schema'

require_relative 'log'
require_relative 'meta'
require_relative 'mixins/json_schema_helper'

module Discoto
  module SortDirection
    ASCENDING = 'ascending'
    DESCENDING = 'descending'

    ALL = [ASCENDING, DESCENDING].freeze
  end

  module SortBy
    CREATED = 'created'
    UPDATED = 'updated'

    ALL = [CREATED, UPDATED].freeze
  end

  class SummarySettings
    extend Mixins::JsonSchemaHelper
    include Logging

    attr_accessor :max_items, :sort_by, :sort_direction

    SCHEMA = object(
      max_items: int(default: -1),
      sort_by: string(enum: SortBy::ALL, default: SortBy::CREATED),
      sort_direction: string(enum: SortDirection::ALL, default: SortDirection::ASCENDING)
    )
    DEFAULT_OPTIONS = create_defaults(SCHEMA).freeze

    def initialize(options = nil)
      errors = self.class.validate_and_insert_defaults(SCHEMA, options)
      unless errors.empty?
        log.warn('Data validation errors in summary settings YAML')
        errors.each do |error|
          log.warn("  #{error}")
        end

        # inject defaults for everything
        options = DEFAULT_OPTIONS
      end

      @max_items = options['max_items']
      @sort_by = options['sort_by']
      @sort_direction = options['sort_direction']
    end

    def to_h
      {
        'max_items' => max_items,
        'sort_by' => sort_by,
        'sort_direction' => sort_direction
      }
    end
  end

  class TopicSettings
    attr_accessor :summary_settings

    DEFAULT_OPTIONS = {
      'summary' => SummarySettings::DEFAULT_OPTIONS
    }.freeze
    META_SETTINGS_MARKER = 'topic-settings'

    def self.new_from_meta(text)
      extracted = Meta.extract_visible(text, META_SETTINGS_MARKER)
      return nil if extracted.nil?

      yaml_match = extracted.match(/```yaml\n(.*)\n```/m)
      if yaml_match.nil?
        options = {}
      else
        yaml_content = yaml_match.captures[0]
        begin
          options = YAML.safe_load(yaml_content)
        rescue StandardError
          options = nil
        end
      end

      options = {} unless options.is_a?(Hash)

      TopicSettings.new(options)
    end

    def initialize(options = nil)
      options ||= DEFAULT_OPTIONS
      @summary_settings = SummarySettings.new(options['summary'] || {})
    end

    def to_h
      {
        'summary' => @summary_settings.to_h
      }
    end

    def update_or_inject_meta(text)
      yaml_data = to_h.to_yaml
      settings_data = <<~TEXT
        <details>
        <summary>Discoto Settings</summary>

        <br/>

        ```yaml
        #{yaml_data}
        ```

        See the [settings schema](https://gitlab.com/gitlab-org/secure/pocs/discoto#settings-schema) for details.

        </details>
      TEXT
      Meta.inject_visible(text, META_SETTINGS_MARKER, settings_data)
    end
  end
end
