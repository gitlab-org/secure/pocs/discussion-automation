# frozen_string_literal: true

require_relative 'items/main_point'
require_relative 'config'

module Discoto
  class QuickActions
    HANDLED_EMOJI = 'robot'
    MANUAL_RESPONSE_TEXT = 'DISCOTO HANDLED:' # noteable_link
    QUICK_ACTION_REGEX = %r{^/discuss\s+[a-zA-Z-]+.*$}.freeze

    attr_accessor :new_sub_topics

    def self.quick_action?(text)
      find_quick_actions(text).count.positive?
    end

    def self.find_quick_actions(text)
      text.scan(QUICK_ACTION_REGEX)
    end

    def self.remove_quick_actions(text)
      text.gsub(QUICK_ACTION_REGEX, '')
    end

    def initialize(loader)
      @loader = loader
      @new_sub_topics = []
    end

    def process(thread)
      quick_actions = {
        'sub-topic' => method(:handle_new_sub_topic),
        'link' => method(:handle_link_issuable)
      }

      thread.notes.each do |note|
        actions = self.class.find_quick_actions(note['body'])
        next unless actions.count.positive?
        next if handled?(thread, note)

        mark_as_handled!(thread, note)
        actions.each do |action_text|
          action_parts = action_text.gsub(%r{^/discuss\s*}, '').strip.split(/\s+/, 2)
          next if action_parts.count.zero?

          action, args = action_parts
          next unless quick_actions.include?(action)

          quick_actions[action].call(note, thread, args)
        end
      end
    end

    def handle_link_issuable(_note, thread, args)
      link = args.split(/\s+/)[0]
      gitlab_uri = URI(Discoto::Config::CONFIG[:gitlab_endpoint])

      begin
        link_route = gitlab_uri.route_to(link)
        # not a relative path on the same host (host will be set)
        return unless link_route.host.nil?
      rescue URI::BadURIError
        return
      end

      match = link_route.path.match(%r{^/(.*)/-/(issues|merge_requests|epics)/(\d+)})
      return unless match

      _, container_path, type_plural, id = match.to_a
      type = type_plural.delete_suffix!('s')
      id = id.to_i

      container_type = (type == 'epic' ? 'group' : 'project')
      container_path.sub!(%r{^groups/}, '') if container_type == 'group'

      existing_topic = @loader.load_topic(type, id, container_type, container_path)
      @new_sub_topics.push(existing_topic)
      thread.add_link!(existing_topic)
    end

    def handle_new_sub_topic(note, thread, args)
      desc = <<~DESC
        This issue is a continuation of the discussion at #{thread.url}

        **NOTE**

        _Although this issue was automatically created, feel free to update the
        description as needed. The `Auto-Summary` heading will be automatically
        regenerated from all inline discussions in this issue as well as all
        sub-topics._
      DESC

      if thread.issuable.type == 'epic'
        mark_as_handled!(thread, note)
        thread.add_note(":#{HANDLED_EMOJI}: Creating sub-topics from epics is not yet supported")
        return
      end

      cinfo = @loader.container_info(thread.issuable)
      new_topic = Items::Topic.create(@loader, cinfo.id, args, description: desc)
      thread.close!(new_topic)

      @new_sub_topics.push(new_topic)
    end

    def handled?(thread, note)
      @loader.note_has_emoji?(thread.issuable, note, HANDLED_EMOJI)
    rescue StandardError
      responded_to?(thread, note)
    end

    # epics don't have an emoji API, so we need another way to tell if
    # we have responded to quick actions. The same applies to mark_as_handled
    # below
    def responded_to?(thread, note)
      found_start = false
      thread.notes.each do |curr_note|
        if note == curr_note
          found_start = true
        elsif found_start
          match = curr_note['body'].match(/^#{MANUAL_RESPONSE_TEXT} \[(\d+)\]\(#note_\d+\)/)
          return true if match && match[1].to_i == note.id
        end
      end

      false
    end

    def mark_as_handled!(thread, note)
      @loader.note_add_emoji(thread.issuable, note, HANDLED_EMOJI)
    rescue StandardError
      thread.add_note("#{MANUAL_RESPONSE_TEXT} [#{note.id}](#note_#{note.id})")
    end
  end
end
