# frozen_string_literal: true

require_relative '../topic_settings'
require_relative '../items/main_point'

module Discoto
  module Mixins
    module Summary
      def summary_title(topic)
        if !confidential? && topic.confidential?
          topic.url
        else
          "#{topic.title} #{topic.url}"
        end
      end

      def summary_lines
        summary_settings = settings.summary_settings
        all_items = main_points + sub_topics

        # sort -> handle direction -> limit # of items

        # sort
        all_items.sort_by! do |item|
          if summary_settings.sort_by == ::Discoto::SortBy::CREATED
            item.created
          elsif summary_settings.sort_by == ::Discoto::SortBy::UPDATED
            item.updated
          end
        end

        # handle direction - default is ascending
        if summary_settings.sort_direction == ::Discoto::SortDirection::DESCENDING
          all_items.reverse!
        end

        # limit number of results
        num_items = 0
        result_lines = []
        all_items.each do |item|
          num_items += 1
          break if (summary_settings.max_items >= 0) && (num_items > summary_settings.max_items)

          if item.is_a?(::Discoto::Items::MainPoint)
            add_main_point_summary(item, result_lines)
          else
            add_sub_topic_summary(item, result_lines)
          end
        end

        result_lines
      end

      def add_main_point_summary(main_point, result_lines)
        result_lines.push("* #{main_point.text} #{main_point.url}")
      end

      def add_sub_topic_summary(sub_topic, result_lines)
        # going from private->public, only link, no summary text
        if sub_topic.confidential? && !confidential?
          result_lines.push("* **TOPIC** #{sub_topic.url}")
          return
        end

        result_lines.push("* **TOPIC** #{sub_topic.title} #{sub_topic.url}")
        child_lines = sub_topic.summary_lines.map do |line|
          if line.match(/\s*\*/)
            # list sub_topic
            "  #{line}"
          else
            line
          end
        end
        result_lines.concat(child_lines)
      end
    end
  end
end
