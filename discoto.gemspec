# frozen_string_literal: true

require_relative 'lib/discoto/version'

Gem::Specification.new do |spec|
  spec.name          = 'discoto'
  spec.version       = Discoto::VERSION
  spec.authors       = ['James Johnson']
  spec.email         = ['jjohnson@gitlab.com']

  spec.summary       = 'DISCussion AUTOmation'
  spec.description   = 'Discoto automates discussions in GitLab issuables'
  spec.homepage      = 'https://gitlab.com/gitlab-org/secure/pocs/discoto'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/secure/pocs/discoto'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/secure/pocs/discoto'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.glob('{exe,bin,lib}/**/*') + %w[README.md LICENSE.txt]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'gitlab', '~> 4.14'
  spec.add_dependency 'json-schema', '~> 2.8'

  spec.add_development_dependency 'pry', '~> 0.13.1'
  spec.add_development_dependency 'pry-byebug', '~> 3.9'
  spec.add_development_dependency 'rspec', '~>3.9.0'
  spec.add_development_dependency 'rspec-mocks', '~>3.9.1'
  spec.add_development_dependency 'rspec-parameterized', '~> 0.4.2'
  spec.add_development_dependency 'rubocop', '~> 1.65.1'
  spec.add_development_dependency 'simplecov', '~> 0.21.2'
end
