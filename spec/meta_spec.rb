# frozen_string_literal: true

require 'rspec'

require 'discoto/meta'

describe Discoto::Meta do
  context '#create_visible_markers' do
    it 'creates correct visible markers' do
      prefix, postfix = Discoto::Meta.create_visible_markers("a type")
      expect(prefix).to eq('<!-- start-discoto-a-type -->')
      expect(postfix).to eq('<!-- end-discoto-a-type -->')

      prefix, postfix = Discoto::Meta.create_visible_markers("a--type")
      expect(prefix).to eq('<!-- start-discoto-a-type -->')
      expect(postfix).to eq('<!-- end-discoto-a-type -->')
    end
  end

  context '#extract_visible' do
    it 'returns nil if no data was previously injected' do
      expect(Discoto::Meta.extract_visible('Does not exist', 'marker')).to eq(nil)
    end

    it 'extracts the data between the markers' do
      data = "Hello\n\n\nworld!\n\n\t"
      marker_type = 'test markers'

      prefix, postfix = Discoto::Meta.create_visible_markers(marker_type)
      markdown = "Previous text\n\n\n" + prefix + data + postfix + "\n\n\nFollowing text"
      expect(Discoto::Meta.extract_visible(markdown, marker_type)).to eq(data)
    end
  end

  context '#inject_visible' do
    it 'appends marked data to the markdown if the markers do not already exist' do
      data = "Hello\n\n\nworld!\n\n\t"
      marker_type = 'test markers'
      markdown = "Previous text\n\n\n"

      injected = Discoto::Meta.inject_visible(markdown, marker_type, data)

      prefix, postfix = Discoto::Meta.create_visible_markers(marker_type)
      expect(injected).to eq(markdown + "\n\n" + prefix + data + postfix)
    end

    it 'updates an existing marked block within the markdown' do
      marker_type = 'test markers'
      prefix, postfix = Discoto::Meta.create_visible_markers(marker_type)

      start_data = "Hello\n\n\nworld!\n\n\t"
      markdown = "Previous text\n\n\n#{prefix}#{start_data}#{postfix}\n\n\nFollowing text"

      new_data = "Goodbye\n\n\nworld!\n\n\t"
      expected_markdown = "Previous text\n\n\n#{prefix}#{new_data}#{postfix}\n\n\nFollowing text"

      updated_markdown = Discoto::Meta.inject_visible(markdown, marker_type, new_data)
      expect(updated_markdown).to eq(expected_markdown)
    end
  end
end
