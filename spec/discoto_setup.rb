require 'rspec'
require 'discoto/config'
require 'discoto/loader'

require_relative 'utils'

RSpec.configure do |rspec|
  # This config option will be enabled by default on RSpec 4,
  # but for reasons of backwards compatibility, you have to
  # set it on RSpec 3.
  #
  # It causes the host group and examples to inherit metadata
  # from the shared context.
  rspec.shared_context_metadata_behavior = :apply_to_host_groups
end

RSpec.shared_context "discoto setup", :shared_context => :metadata do
  let(:client) {
    Gitlab.client(
      endpoint: "#{Discoto::Config::CONFIG[:gitlab_endpoint]}/api/v4",
      private_token: Discoto::Config::CONFIG[:gitlab_token]
    )
  }
  let(:test_group_path) { Discoto::Config::CONFIG[:groups].first }
  let(:test_group) { client.group(test_group_path) }

  let(:test_project) do
    begin
      client.project("#{test_group.path}/test-project-public")
    rescue
      client.create_project('test-project-public', { 'namespace_id': test_group.id, 'visibility': 'public' })
    end
  end
  let(:test_project_private) do
    begin
      client.project("#{test_group.path}/test-project-private")
    rescue
      client.create_project('test-project-private', { 'namespace_id': test_group.id, 'visibility': 'private' })
    end
  end

  let(:loader) do
    Discoto::Config::CONFIG[:projects] = [
      test_project.path_with_namespace,
      test_project_private.path_with_namespace,
    ]
    Discoto::Loader.new(client, Discoto::Config::CONFIG)
  end

  after(:each) do
    client.issues(test_project.id, { 'state': 'opened' }).auto_paginate.each do |issue|
      client.edit_issue(issue.project_id, issue.iid, { state_event: 'close' })
    end

    client.merge_requests(test_project.id, { 'state': 'opened' }).auto_paginate.each do |mr|
      client.update_merge_request(mr.project_id, mr.iid, { state_event: 'close' })
    end

    client.branches(test_project.id).auto_paginate.each do |branch|
      next if branch.default
      client.delete_branch(test_project.id, branch.name)
    end
  end
end

RSpec.configure do |rspec|
  rspec.include_context "discoto setup", :include_shared => true
end
