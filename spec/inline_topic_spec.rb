# frozen_string_literal: true

require 'rspec'
require 'rspec/mocks'

require 'discoto/items/inline_topic'
require 'discoto/items/topic'

require_relative 'discoto_setup'

describe 'InlineTopic' do
  include_context 'discoto setup'

  describe '#process' do
    it 'Sets the parent of sub-topics to this topic' do
      linked_title = "LinkedTopic"
      linked_point = "LinkedPoint"
      linked_issue = TestUtils.new_issue(
        client,
        test_project,
        title: linked_title,
        labels: [],
        threads: [
          [
            "* point: #{linked_point}"
          ]
        ]
      )

      root_title = "RootTopic"
      inline_title = "InlineTopic"
      root_issue = TestUtils.new_issue(client, test_project, title: root_title, threads: [
        [
          "# Topic: #{inline_title}",
          "/discuss link #{linked_issue.web_url}",
        ]
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      root_topic = topics.first

      all_topics = root_topic.process

      expect(root_topic.title).to eq(root_title)

      expect(root_topic.sub_topics.count).to eq(1)
      inline_topic = root_topic.sub_topics.first
      expect(inline_topic.title).to eq(inline_title)

      expect(inline_topic.sub_topics.count).to eq(1)
      linked_topic = inline_topic.sub_topics.first
      expect(linked_topic.title)

      expect(all_topics.count).to eq(3)

      lines = root_topic.summary_lines

      expect(lines[0]).to start_with("* **TOPIC** #{inline_title}")
      expect(lines[1]).to start_with("  * **TOPIC** #{linked_title}")
      expect(lines[2]).to start_with("    * #{linked_point}")
    end

    it 'Does not allow duplicated sub-topics' do
      linked_issue = TestUtils.new_issue(client, test_project, labels: [])
      root_issue = TestUtils.new_issue(client, test_project, title: 'Root', threads: [
        # link it twice
        [
          "# Topic: SubTopic",
          "/discuss link #{linked_issue.web_url}",
          "/discuss link #{linked_issue.web_url}",
        ]
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      # will process quick actions, insert json meta links, etc.
      # Verify at this stage...
      all_topics = topics.first.process
      expect(all_topics.count).to eq(3)
      root_topic, inline_topic, linked = all_topics
      expect(root_topic.title).to eq('Root')
      expect(root_topic.sub_topics.count).to eq(1)

      expect(inline_topic.title).to eq('SubTopic')
      expect(inline_topic.sub_topics.count).to eq(1) # 2 == it allowed a duplicated link

      # .. and at this stage (simulate the next "run" of discoto after json meta
      # data has already been added)
      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      all_topics = topics.first.process
      expect(all_topics.count).to eq(3)
      root_topic, inline_topic, linked = all_topics
      expect(root_topic.title).to eq('Root')
      expect(root_topic.sub_topics.count).to eq(1)

      expect(inline_topic.title).to eq('SubTopic')
      expect(inline_topic.sub_topics.count).to eq(1) # 2 == it allowed a duplicated link
    end
  end
end
