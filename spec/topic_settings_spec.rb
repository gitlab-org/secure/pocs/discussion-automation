# frozen_string_literal: true

require 'rspec'
require 'yaml'

require 'discoto/topic_settings'

describe Discoto::SummarySettings do
  context '#initialize' do
    it 'accepts data that conforms to the JSON schema' do
      data = {
        'max_items' => 10,
        'sort_direction' => Discoto::SortDirection::DESCENDING,
        'sort_by' => Discoto::SortBy::UPDATED,
      }
      settings = Discoto::SummarySettings.new(data)

      expect(settings.max_items).to eq(data['max_items'])
      expect(settings.sort_by).to eq(data['sort_by'])
      expect(settings.sort_direction).to eq(data['sort_direction'])
    end

    it 'inserts defaults for missing values' do
      data = {}
      settings = Discoto::SummarySettings.new(data)

      schema = Discoto::SummarySettings::SCHEMA
      expect(settings.max_items).to eq(schema['properties']['max_items']['default'])
      expect(settings.sort_by).to eq(schema['properties']['sort_by']['default'])
      expect(settings.sort_direction).to eq(schema['properties']['sort_direction']['default'])
    end

    it 'inserts defaults for invalid values' do
      data = {
        'max_items' => 'hello',
        'sort_by' => 1,
        'sort_direction' => {'hello' => 10},
      }
      settings = Discoto::SummarySettings.new(data)

      schema = Discoto::SummarySettings::SCHEMA
      expect(settings.max_items).to eq(schema['properties']['max_items']['default'])
      expect(settings.sort_by).to eq(schema['properties']['sort_by']['default'])
      expect(settings.sort_direction).to eq(schema['properties']['sort_direction']['default'])
    end
  end

  describe '#to_h' do
    it 'creates a hash matching the set values' do
      settings = Discoto::SummarySettings.new
      hashed = settings.to_h
      expect(hashed).to eq(Discoto::SummarySettings::DEFAULT_OPTIONS)
      expect(settings.max_items).to eq(hashed['max_items'])
      expect(settings.sort_by).to eq(hashed['sort_by'])
      expect(settings.sort_direction).to eq(hashed['sort_direction'])
    end
  end
end

describe Discoto::TopicSettings do
  context '#self.new_from_meta' do
    it 'Creates settings from embedded metadata' do
      settings_hash = {
        "summary" => {
          "max_items" => 100,
          "sort_by" => Discoto::SortBy::UPDATED,
          "sort_direction" => Discoto::SortDirection::DESCENDING,
        }
      }

      text = <<EOF
HELLO
WORLD
<!-- start-discoto-topic-settings -->
```yaml
#{settings_hash.to_yaml}
```
<!-- end-discoto-topic-settings -->
EOF
      
      settings = Discoto::TopicSettings.new_from_meta(text)
      expect(settings.to_h).to eq(settings_hash)
    end

    it 'Returns nil if settings do not exist in the text' do
      text = "HEllo\nWorld\n"
      settings = Discoto::TopicSettings.new_from_meta(text)
      expect(settings).to eq(nil)
    end
  end

  context '#update_or_inject_meta' do
    it "Injects metadata into the provided text if it doesn't exist" do
      text = "hello\nthere\n"
      settings = Discoto::TopicSettings.new
      updated_text = settings.update_or_inject_meta(text)

      expect(updated_text).to include("```yaml\n#{settings.to_h.to_yaml}\n```")
    end

    it "Updates existing data in the yaml" do
      settings_hash = {
        "summary" => {
          "max_items" => 111,
          "sort_by" => Discoto::SortBy::UPDATED,
          "sort_direction" => Discoto::SortDirection::DESCENDING,
        }
      }
      settings = Discoto::TopicSettings.new(settings_hash)
      initial_text = settings.update_or_inject_meta("Hello\nthere\n")
      expect(initial_text).to include('111')

      settings.summary_settings.max_items = 222
      updated_text = settings.update_or_inject_meta(initial_text)

      expect(updated_text).to include("```yaml\n#{settings.to_h.to_yaml}\n```")
      expect(updated_text).to_not include('111')
      expect(updated_text).to include('222')
    end
  end
end
