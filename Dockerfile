FROM ruby:3.2-alpine3.20 as BUILDER

COPY . /discoto
WORKDIR /discoto
RUN apk update \
    && apk add make gcc libc-dev \
    && bundler_version=$(grep -A1 "BUNDLED WITH" Gemfile.lock | tail -n +2) \
    && gem install bundler -v "${bundler_version}" \
    && bundler install \
    && rake build

# -----------------------------------------------------------------------------
# FINAL STAGE -----------------------------------------------------------------
# -----------------------------------------------------------------------------

FROM ruby:3.2-alpine3.20
COPY --from=BUILDER /discoto/pkg/*.gem /discoto.gem

RUN apk update \
    && apk add make gcc libc-dev git \
    && gem install /discoto.gem \
    && rm /discoto.gem
